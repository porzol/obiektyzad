package ZadaniaSobotaFourthNov.Zadanie14;

public class Paczka {
    private String odbiorca;
    private String nadawca;
    private boolean czyWyslana;
    private boolean czyJestZawartosc;

    public Paczka() {
    }

    public Paczka(String odbiorca, boolean czyJestZawartosc) {
        this.odbiorca = odbiorca;
        this.czyJestZawartosc = czyJestZawartosc;
    }

    public String getOdbiorca() {
        return odbiorca;
    }

    public String getNadawca() {
        return nadawca;
    }

    public boolean isCzyWyslana() {
        return czyWyslana;
    }

    public boolean isCzyJestZawartosc() {
        return czyJestZawartosc;
    }

    public void setOdbiorca(String odbiorca) {
        this.odbiorca = odbiorca;
    }

    public void setNadawca(String nadawca) {
        this.nadawca = nadawca;
    }

    public void setCzyWyslana(boolean czyWyslana) {
        this.czyWyslana = czyWyslana;
    }

    public void setCzyJestZawartosc(boolean czyJestZawartosc) {
        this.czyJestZawartosc = czyJestZawartosc;
    }
    public void wyslij (){
        if(czyJestZawartosc && odbiorca != null){
            System.out.println("Paczka wyslana");
            czyWyslana = true;
        }else System.out.println("brakuje odbiorcy lub zawartosci");
    }
    public void wyslijPolecony (){
        if(czyJestZawartosc && odbiorca != null && nadawca != null){
            System.out.println("Paczka polecona wyslana");
            czyWyslana = true;
        }else System.out.println("brakuje odbiorcy lub zawartosci");
    }


}
