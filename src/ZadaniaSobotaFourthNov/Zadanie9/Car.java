package ZadaniaSobotaFourthNov.Zadanie9;

public class Car {
    private String carName;
    private int liczSiedz;
    private int pojSilnika;
    private int lKoni;
    private int predkoscObecna;
    private int przebieg;
    private int rok_produkcji;
    private int passangers;

    public Car(String carName, int liczSiedz, int pojSilnika, int lKoni, int predkoscObecna, int przebieg, int rok_produkcji, int passangers) {
        this.carName = carName;
        this.liczSiedz = liczSiedz;
        this.pojSilnika = pojSilnika;
        this.lKoni = lKoni;
        this.predkoscObecna = predkoscObecna;
        this.przebieg = przebieg;
        this.rok_produkcji = rok_produkcji;
        this.passangers = passangers;
    }

    public int getPassangers() {
        return passangers;
    }

    public String getCarName() {
        return carName;
    }
    public void addPassenger(){
        if(passangers < liczSiedz) {
            passangers++;
        }else{
            System.out.println("Nie ma miejsc w samochodzie");
        }
    }
    public void removePassenger(){
        passangers--;
    }
    public void speedIncrease(int predk){
        if(passangers > 0){
        predkoscObecna =+ predk;
        }else{
            System.out.println("Brakuje kierwcy");
        }
    }
    public void speedDecrease(int predk){
        if(passangers > 0){
            predkoscObecna =- predk;
        }else{
            System.out.println("Brakuje kierwcy");
        }
    }
}
