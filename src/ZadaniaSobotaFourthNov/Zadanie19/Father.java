package ZadaniaSobotaFourthNov.Zadanie19;

public class Father extends FamilyMember {
    public Father(String name) {
        super(name);
    }

    @Override
    public void introduce() {
        System.out.println("i am father");
    }
}
