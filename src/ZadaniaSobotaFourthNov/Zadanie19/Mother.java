package ZadaniaSobotaFourthNov.Zadanie19;

public class Mother extends FamilyMember {
    public Mother(String name) {
        super(name);
    }

    @Override
    public void introduce() {
        System.out.println("i am mother");
    }
}
