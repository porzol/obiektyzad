package ZadaniaSobotaFourthNov.Zadanie19;

public class Daughter extends FamilyMember{

    public Daughter(String name) {
        super(name);
    }

    @Override
    public void introduce() {
        System.out.println("i am daughter");
    }


}
