package ZadaniaSobotaFourthNov.Zadanie19;

public class Son extends FamilyMember {
    public Son(String name) {
        super(name);
    }

    @Override
    public void introduce() {
        System.out.println("i am son");
    }
}
