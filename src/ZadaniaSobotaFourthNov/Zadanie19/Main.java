package ZadaniaSobotaFourthNov.Zadanie19;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        FamilyMember son = new Son("Tomek");
        FamilyMember daughter = new Daughter("Ala");
        FamilyMember mother = new Mother("Anna");
        FamilyMember father = new Father("Jacek");

        List <FamilyMember> rodzina = new ArrayList <>();
        rodzina.add(son);
        rodzina.add(daughter);
        rodzina.add(father);
        rodzina.add(mother);
        System.out.println(rodzina.indexOf(father));

        for (FamilyMember member:rodzina) {
            member.introduce();
        }
    }
}
