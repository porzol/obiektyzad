package ZadaniaSobotaFourthNov.Zadanie19;

public class FamilyMember {
    String name;

    public FamilyMember(String name) {
        this.name = name;
    }

    public void introduce() {
        System.out.println("I am just a simple family member");
    }

}
