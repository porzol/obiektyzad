package ZadaniaSobotaFourthNov.Zadanie20;

public interface IFigure {
    double pole();
    double obwod();
}
