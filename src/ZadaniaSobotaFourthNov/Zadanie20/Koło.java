package ZadaniaSobotaFourthNov.Zadanie20;

public class Koło extends CFigure{
private double r;

    public Koło(double r) {
        this.r = r;
    }

    @Override
    public double pole() {
        return Math.PI * r * r;
    }

    @Override
    public double obwod() {
        return 2 * Math.PI * r;
    }
}
