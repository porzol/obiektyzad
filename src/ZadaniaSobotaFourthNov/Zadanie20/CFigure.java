package ZadaniaSobotaFourthNov.Zadanie20;

public abstract class CFigure implements IFigure {
    @Override
    public abstract double pole();

    @Override
    public abstract double obwod();

}
