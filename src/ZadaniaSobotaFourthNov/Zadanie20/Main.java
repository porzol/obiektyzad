package ZadaniaSobotaFourthNov.Zadanie20;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        CFigure kolo = new Koło(4);
        CFigure kwadrat = new Kwadrat(3);

        List <CFigure> lista = new ArrayList<>();
        lista.add(kolo);
        lista.add(kwadrat);

        for (CFigure figura:lista) {
            System.out.println("pole:" + figura.pole() + " obwod:" + figura.obwod());
        }
    }
}
