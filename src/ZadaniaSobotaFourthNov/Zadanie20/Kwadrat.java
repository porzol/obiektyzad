package ZadaniaSobotaFourthNov.Zadanie20;

public class Kwadrat extends CFigure {
    private double a;

    public Kwadrat(double a) {
        this.a = a;
    }

    @Override
    public double pole() {
        return a*a;
    }

    @Override
    public double obwod() {
        return a*4;
    }
}
