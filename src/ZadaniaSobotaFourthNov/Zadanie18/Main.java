package ZadaniaSobotaFourthNov.Zadanie18;

public class Main {
    public static void main(String[] args) {
        Person person = new Person("Tom", "Nowy", 17);

        Club club = new Club();

        try{
            club.enter(person);
        }
        catch (NoAdultException nae){
            System.out.println(person.getImie() + " został wyrzucony." + nae.getMessage());
        }
    }
}
