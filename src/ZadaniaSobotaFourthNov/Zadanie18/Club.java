package ZadaniaSobotaFourthNov.Zadanie18;

public class Club {
    public void enter(Person person) throws NoAdultException{
        if (person.getWiek() < 18){
            throw new NoAdultException();
        }else
        System.out.println(person.getImie() + " wchodzi do klubu");
    }
}
