package ZadaniaSobotaFourthNov.Zadanie13;

import java.util.ArrayList;
import java.util.List;

public class Student {
    private long numer_indeksu;
    private String imie;
    private String nazwisko;
    private List<Integer> lista_ocen;

    @Override
    public String toString() {
        return "Student{" +
                "numer_indeksu=" + numer_indeksu +
                ", imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", lista_ocen=" + lista_ocen;
    }

    public Student(long numer_indeksu, String imie, String nazwisko, List<Integer> lista_ocen) {
        this.numer_indeksu = numer_indeksu;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.lista_ocen = lista_ocen;
    }


    public long getNumer_indeksu() {
        return numer_indeksu;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public List<Integer> getLista_ocen() {
        return lista_ocen;
    }

    public void setNumer_indeksu(long numer_indeksu) {
        this.numer_indeksu = numer_indeksu;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public void setLista_ocen(List<Integer> lista_ocen) {
        this.lista_ocen = lista_ocen;
    }
    public double srednia (List<Integer> lista){
        double suma = 0;
        double srednia = 0;
        for (int i = 0; i < lista.size(); i++) {
            suma += lista.get(i);
        }
        srednia = Double.valueOf(suma/lista.size());

        return srednia;
    }
    public boolean czyZdales (List<Integer> lista){
        if (lista.contains(1) || lista.contains(2)){
            return false;
        }else

        return true;
    }
}




