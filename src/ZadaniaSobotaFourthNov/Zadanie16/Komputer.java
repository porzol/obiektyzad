package ZadaniaSobotaFourthNov.Zadanie16;

public class Komputer {
    private String pc_name;
    private double processor_power;
    private int disc_size;
    private int ram_size;
    private String graphicsCardName;

    public Komputer(String pc_name, double processor_power, int disc_size, int ram_size) {
        this.pc_name = pc_name;
        this.processor_power = processor_power;
        this.disc_size = disc_size;
        this.ram_size = ram_size;
    }

    public boolean czyJestKomputeremDoGier () {
        if(podajMocObliczeniowa() > 10){
            System.out.println("To jest komputer do grania");
            return true;
        }else
            System.out.println("To nie jest komputer do grania");
            return false;
    }
    public double podajMocObliczeniowa () {
        System.out.println("Moc obliczeniowa to:");
        if (!czyPosiadaKarteGraficzna()) {
            return getProcessor_power() * 0.95 + (0.3 * getRam_size());
        } else return getProcessor_power() * 1.45 + (0.25 * getRam_size());
    }
    public boolean czyPosiadaKarteGraficzna (){
        if(graphicsCardName != null){
            return true;
        }else
            return false;
    }
    public void setGraphicsCardName(String nazwaKarty){
        graphicsCardName = nazwaKarty;
    }

    public void setPc_name(String pc_name) {
        this.pc_name = pc_name;
    }

    public void setProcessor_power(double processor_power) {
        this.processor_power = processor_power;
    }

    public void setDisc_size(int disc_size) {
        this.disc_size = disc_size;
    }

    public void setRam_size(int ram_size) {
        this.ram_size = ram_size;
    }

    public String getPc_name() {
        return pc_name;
    }

    public double getProcessor_power() {
        return processor_power;
    }

    public int getDisc_size() {
        return disc_size;
    }

    public int getRam_size() {
        return ram_size;
    }
}
