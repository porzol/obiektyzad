package ZadaniaSobotaFourthNov.Zadanie15;

public class Telefon {
    private boolean czyDzwoni;
    private long nr_tel;
    private long wybrany_nr;
    private String zawartWyswietlacza;

    @Override
    public String toString() {
        return "Telefon{" +
                "Dzwoni" + czyDzwoni +
                ", nr_tel=" + nr_tel +
                '}';
    }

    public void setCzyDzwoni(boolean czyDzwoni) {
        this.czyDzwoni = czyDzwoni;
    }

    public void setNr_tel(long nr_tel) {
        this.nr_tel = nr_tel;
    }

    public boolean isCzyDzwoni() {
        return czyDzwoni;
    }

    public long getNr_tel() {
        return nr_tel;
    }
    public void zadzwon(long wybrany_nr) {
        if (!czyDzwoni) {
            czyDzwoni = true;
            setZawartWyswietlacza("Rozmowa z " + wybrany_nr);
            System.out.println("Dzwonie gdzies");
        } else {
            System.out.println("Juz gdzies dzwonie");
        }
    }

    public void setZawartWyswietlacza(String zawartWyswietlacza) {
        this.zawartWyswietlacza = zawartWyswietlacza;
    }

    public void rozlacz() {
        if (czyDzwoni) {
            czyDzwoni = false;
            zawartWyswietlacza = "";
            System.out.println("Rozlaczam polaczeznie");
        } else {
            System.out.println("Nie moge rozlaczyc, nie ma polaczenia");
        }
    }

}
