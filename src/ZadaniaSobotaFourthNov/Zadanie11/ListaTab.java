package ZadaniaSobotaFourthNov.Zadanie11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListaTab {
    public static void main(String[] args) {
        List<Integer> lista = new ArrayList<>(Arrays.asList(2,3,5,6,10));
        System.out.println(sredniaEach(lista));
        List<Boolean> list = new ArrayList<>(Arrays.asList(false,false,false,false));
//        System.out.println(negowanie(list));
          System.out.println(alternatywa(list));
    }

    private static int suma (List<Integer> tab){
        int suma = 0;
        for(int i = 0; i<tab.size(); i++){
            suma = suma + tab.get(i);
        }
        return suma;
    }
    private static double sumaEach (List<Integer> tab){
        double suma = 0;
        for(Integer wartosc:tab){
            suma += wartosc;
        }
        return suma;
    }
    private static int iloczyn (List<Integer> tab){
        int suma = 1;
        for(int i = 0; i<tab.size(); i++){
            suma = suma * tab.get(i);
        }
        return suma;
    }
    private static int iloczynEach (List<Integer> tab){
        int suma = 1;
        for(Integer wartosc:tab){
            suma =suma * wartosc;
        }
        return suma;
    }
    private static double srednia (List<Integer> tab){
        double srednia = 0;
        srednia = suma(tab)/tab.size();
        return srednia;
    }
    private static double sredniaEach (List<Integer> tab){
        return sumaEach(tab)/tab.size();
    }
    private static List<Boolean> negowanie(List<Boolean> tab){
        boolean tmp = false;
        for (int i = 0; i<tab.size(); i++) {
            tmp = !(tab.get(i));
            tab.remove(i);
            tab.add(i,tmp);
        }
        return  tab;
    }
    private static boolean koniunkcja(List<Boolean> tab){
        boolean tmp = true;
        for (int i = 0; i<tab.size(); i++) {
          tmp = tmp && tab.get(i);
        }
        return tmp;
    }
    private static boolean alternatywa(List<Boolean> tab){
        boolean tmp = false;
        for (Boolean wartosc:tab) {
            tmp = tmp || wartosc;
        }
        return tmp;
    }


}
