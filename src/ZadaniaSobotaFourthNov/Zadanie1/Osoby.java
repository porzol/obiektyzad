package ZadaniaSobotaFourthNov.Zadanie1;//Zadanie 1:
//        Dodaj klasę Osoba o polach imie i wiek.
//        Utwórz 3 osoby o różnych imionach i wieku.
//        Wypisz ich dane w postaci:
//        “Jestem <imie>, mam <wiek> lat.”
//
//        a) stwórz do tego metodę printYourNameAndAge
//        b) wypisz wartości używając metody toString
//        c) wypisz dane używając metod getterów i setterów

public class Osoby {
    private String imie;
    private int wiek;

    public Osoby(String imie, int wiek) {
        this.imie = imie;
        this.wiek = wiek;
    }

    public String getImie() {
        return imie;
    }

    public int getWiek() {
        return wiek;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    public String printYourNameAndAge() {
        return "Mam na imie " + imie + " i mam " + wiek + " lat.";
    }

    @Override
    public String toString() {
        return "Osoby{" +
                "imie='" + imie + '\'' +
                ", wiek=" + wiek +
                '}';
    }
}
