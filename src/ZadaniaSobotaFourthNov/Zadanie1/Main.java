package ZadaniaSobotaFourthNov.Zadanie1;

public class Main {
    public static void main(String[] args) {
        Osoby osoba1 = new Osoby("Tomek", 20);
        Osoby osoba2 = new Osoby("Rafał", 30);
        Osoby osoba3 = new Osoby("Grzegorz", 25);


        System.out.println(osoba1.printYourNameAndAge());
        System.out.println(osoba2.printYourNameAndAge());
        System.out.println(osoba3.printYourNameAndAge());
        //b
        System.out.println(osoba1.getImie());
        System.out.println(osoba1.getWiek());
        //c
        System.out.println(osoba1);
    }

}
