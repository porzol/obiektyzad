package ZadaniaSobotaFourthNov.Zadanie4;//     Zadanie 4:
//        Napisz program w którym jest jedna klasa 'ZadaniaSobotaFourthNov.Zadanie4.Calculator'.
//        Klasa reprezentuje model kalkulatora. Klasa powinna posiadać metodę:
//        -   addTwoNumbers - która przyjmuje dwa parametry i zwraca wynik dodawania
//        -   substractTwoNumbers - która przyjmuje dwa parametry i zwraca wynik odejmowania
//        -   multiplyTwoNumbers - która przyjmuje dwa parametry i zwraca wynik mnożenia
//        -   divideTwoNumbers - która przyjmuje dwa parametry i zwraca wynik dzielenia
//
//        Wszystkie metody zwracają wartości. Stwórz maina, a w nim jedną instancję klasy ZadaniaSobotaFourthNov.Zadanie4.Calculator, a następnie przetestuj działanie wszystkich metod.

public class Calculator {

public double addTwoNumbers(double a, double b){
    return a+b;
}
public double substractTwoNumbers (double a, double b){
    return a-b;
}
public double multiplyTwoNumbers (double a, double b){
    return a*b;
}
public double divideTwoNumbers (double a, double b) throws ArithmeticException {
if (b == 0){
    throw new ArithmeticException("Dzielisz przez zero");
}
    return a/b;
}

}



