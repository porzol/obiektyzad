package ZadaniaSobotaFourthNov.Zadanie2;//Zadanie 2:
//        Utwórz klasę ZadaniaSobotaFourthNov.Zadanie2.TeddyBear, który będzie miał pole prywatne String name; Dodaj konstruktor, który przyjmie za parametr imię.
//        Co to znaczy że pole jest prywatne?
//        Dodaj metodę, która wyświetli imię misia;

public class TeddyBear {

    private String name;

    public TeddyBear(String imie) {
        this.name = imie;
    }

    public String getName() {
        return name;
    }


    public String wyswietlImie() {
        return getName();
    }
}
