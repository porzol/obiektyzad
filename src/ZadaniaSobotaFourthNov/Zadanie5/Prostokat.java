package ZadaniaSobotaFourthNov.Zadanie5;

public class Prostokat {
    private double a;
    private double b;

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public Prostokat(double a, double b) {
        this.a = a;
        this.b = b;
    }
    public double obwod(double bok, double bok2){
        return (bok*2) + bok2;
    }
    public double pole(double bok, double bok2){
        return bok*bok2;
    }
}
