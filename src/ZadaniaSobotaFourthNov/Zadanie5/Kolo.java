package ZadaniaSobotaFourthNov.Zadanie5;

public class Kolo {
    private double promien;

    public Kolo(double promien) {
        this.promien = promien;
    }

    public double getPromien() {
        return promien;
    }

    public double obliczPole() {
        return Math.sqrt(Math.PI * promien);
    }


    public double obliczObwod() {
        return 2 * Math.PI * promien;
    }
}
