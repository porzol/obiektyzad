package ZadaniaSobotaFourthNov.Zadanie5;

public class KwadratObj {
    private double krawedz = 0;

    public double getKrawedz() {
        return krawedz;
    }

    public KwadratObj(double krawedz) {
        this.krawedz = krawedz;
    }
    public double obwod(double bok){
        return bok*4;
    }
    public double pole(double bok){
        return bok*bok;
    }
}
