package ZadaniaNiedziela.Mieszkancy;

public class Peasant extends Citizen {
    public Peasant(String imie) {
        super(imie);
    }

    @Override
    public boolean canVote() {
        return true;
    }
}
