package ZadaniaNiedziela.Mieszkancy;

import java.util.ArrayList;
import java.util.List;

public class Town {
    private List<Citizen> citizens = new ArrayList<>();

    public void addCitizen(Citizen citizen){
        citizens.add(citizen);
    }
    public int howManyCanVote() {
        int counter = 0;
        for (Citizen c : citizens ) {
            if(c.canVote()) counter++;
        }
        return counter;
    }
}
