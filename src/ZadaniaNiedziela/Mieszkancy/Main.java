package ZadaniaNiedziela.Mieszkancy;

public class Main {
    public static void main(String[] args) {
        Town town = new Town();

        town.addCitizen(new Peasant("Tom"));
        town.addCitizen(new Peasant("Rafal"));
        town.addCitizen(new Townsmen("Greg"));

        System.out.println(town.howManyCanVote());
    }
}
