package ZadaniaNiedziela.Mieszkancy;

public class Townsmen extends Citizen{
    public Townsmen(String imie) {
        super(imie);
    }

    @Override
    public boolean canVote() {
        return true;
    }
}
