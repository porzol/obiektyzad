package ZadaniaNiedziela.Studenciaki;
//  Zadanie 25:
//          a. Utwór klasę Student o polach: (long) numerIndeksu, imie, nazwisko oraz:
//          b. Utwórz kilku studentów i dodaj do mapy (HashMap).
//          c. sprawdź, czy mapa zawiera studenta o indeksie 100200,
//          d. wypisz studenta o indeksie 100400
//          e. wypisz liczbę studentów
//          f. wypisz wszystkich studentów
//          g**. zmień implementacje na TreeMap

public class Student {

    long nrIndeksu;
    String imie;
    String nazwisko;

    public Student(long nrIndeksu, String imie, String nazwisko) {
        this.nrIndeksu = nrIndeksu;
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

    public long getNrIndeksu() {
        return nrIndeksu;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }
}
