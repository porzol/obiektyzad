package ZadaniaNiedziela.DateProvider;

public interface DateProvider {
    int naxtInt();
    String nextString();
}
