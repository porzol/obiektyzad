package ZadaniaNiedziela.DateProvider;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Jakiego progamu użyjesz: 1 - stayczny, 2 - konsolowy");
        int i = sc.nextInt();
        DateProvider dp = null;
        if (i == 1){
            dp = new StaticDataProvider();
        }else if (i == 2){
            dp = new ConsoleDataProvider();
        }else if (i == 3){
            dp = new RandomDataProvider();
        }

        if (dp != null){
            int wiek = dp.naxtInt();
            String name = dp.nextString();
            System.out.print("Mam: " + wiek);
            System.out.println(" i mam na imie " + name);
        }
    }
}
