package ZadaniaNiedziela.DateProvider;

public class StaticDataProvider implements DateProvider {
    @Override
    public int naxtInt() {
        return 25;
    }

    @Override
    public String nextString() {
        return "Jan";
    }
}
