package ZadaniaNiedziela.DateProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomDataProvider implements DateProvider {

    private Random rand = new Random();
    private List<String> imiona = new ArrayList<>();

    public RandomDataProvider (){
        imiona.add("Tomek");
        imiona.add("Radek");
        imiona.add("Maciek");

    }
    @Override
    public int naxtInt() {
        Random los = new Random();
        int wiek = los.nextInt(100) + 1;
        return wiek;
    }

    @Override
    public String nextString() {
        return imiona.get(rand.nextInt(imiona.size()));
    }
}
