package ZadaniaNiedziela.DateProvider;

import java.util.Scanner;

public class ConsoleDataProvider implements DateProvider {


    @Override
    public int naxtInt() {
        System.out.println("podaj wiek: ");
        Scanner sc = new Scanner(System.in);
        return sc.nextInt();
    }

    @Override
    public String nextString() {
        System.out.println("podaj wiek: ");
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }
}
