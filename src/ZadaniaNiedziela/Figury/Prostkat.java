package ZadaniaNiedziela.Figury;

public class Prostkat implements Figura {
    private double bok;
    private double bok1;

    public Prostkat(double bok, double bok1) {
        this.bok = bok;
        this.bok1 = bok1;
    }

    @Override
    public double obliczPole() {
        return bok * bok1;
    }

    @Override
    public double obliczObwod() {
        return (bok + bok1)*2;
    }
}
