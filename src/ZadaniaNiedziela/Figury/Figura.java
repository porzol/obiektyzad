package ZadaniaNiedziela.Figury;

public interface Figura {

    double obliczPole();
    double obliczObwod();

}
