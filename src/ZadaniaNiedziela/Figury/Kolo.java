package ZadaniaNiedziela.Figury;

public class Kolo implements Figura {
    private double promien;

    public Kolo(double promien) {
        this.promien = promien;
    }

    @Override
    public double obliczPole() {
        return Math.sqrt(Math.PI * promien);
    }

    @Override
    public double obliczObwod() {
        return 2 * Math.PI * promien;
    }
}
