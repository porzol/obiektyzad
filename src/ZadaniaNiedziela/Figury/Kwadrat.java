package ZadaniaNiedziela.Figury;

public class Kwadrat implements Figura {
    private double bok;

    public Kwadrat(double bok) {
        this.bok = bok;
    }

    @Override
    public double obliczPole() {
        return Math.sqrt(bok);
    }

    @Override
    public double obliczObwod() {
        return bok * 4;
    }
}