package ZadaniaNiedziela.Figury;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Figura kolo = new Kolo(5);
        Figura prostokat = new Prostkat(2,4);

        List figury = new ArrayList();
        figury.add(kolo);
        figury.add(prostokat);

        for (int i = 0; i < figury.size(); i++) {
            if(figury.get(i) instanceof Figura) {
                System.out.println(((Figura) figury.get(i)).obliczObwod());
                System.out.println(((Figura) figury.get(i)).obliczPole());
            }
        }
    }



}
