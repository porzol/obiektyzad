package ZadaniaNiedziela.HashMapa;
import java.util.*;
public class ParaLiczb {
    int a;
    int b;

    public ParaLiczb(int a, int b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public String toString() {
        return "ParaLiczb{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParaLiczb paraLiczb = (ParaLiczb) o;

        if (a != paraLiczb.a) return false;
        return b == paraLiczb.b;
    }

    @Override
    public int hashCode() {
        int result = a;
        result = 31 * result + b;
        return result;
    }

    public static void main(String[] args) {
        Set<ParaLiczb> para = new HashSet<>();
        para.add(new ParaLiczb(1,4));
        para.add(new ParaLiczb(2,5));
        para.add(new ParaLiczb(7,2));
        para.add(new ParaLiczb(4,2));
        para.add(new ParaLiczb(4,2));

        for(ParaLiczb e : para){
            System.out.println(e);
        }
    }
}
